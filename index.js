function F1() {
    console.log("this is function 1");
}
function F2(arg) {
    console.log("this is function 2: ", arg);
}
function F3(arg) {
    console.log("this is function 3: ", arg);
    return "f3 ahahahaha"
}
F1();
F2("abrra cadabra");
console.log("F3 return: ", F3("kedabra?"))

const F4 = function F5(arg) {

    console.log("F4 + F5", arg);
    if (arg == 0) {
        return 0;
    } else {
        F5(0);
    }
}
F4(1);

const F6 = () => {
    console.log("This is arrow F6");
}
F6();